#!/bin/bash

## version
VERSION="1.1.0"

extract_folder_name_from_repository_path () {
  echo $1 | cut -f5 -d\/ | cut -f1 -d.
}

writes_temporary_list_repos_address () {
  echo /tmp/$(date +%Y%m%d-%Hh%Mm%Ss)
}

get_repos_based_on_pagination () {
  curl $repository_address?page=$1 | grep -i git_url | cut -f4 -d\"
}

download_or_pull_repo () {
  repo_folder=$(extract_folder_name_from_repository_path $1)
  if [ -d $repo_folder ]; then
    echo Updating $repo_folder
    cd $repo_folder
    git pull
    cd ..
  else
    echo Cloning $repo_folder
    git clone $i
  fi
}

check_for_tmp_count_repos () {
  if [ $(cat $temporary_list_repos | wc -l) -gt 0 ]
  then
    echo 1
  else
    echo 0
  fi
}

download_repos_from_page () {
  get_repos_based_on_pagination $1 > $temporary_list_repos
  pagination_is_not_empty=$(check_for_tmp_count_repos)
  if [ $pagination_is_not_empty = 1 ]; then
    loop_through_temporary_file_and_download
  fi
}

loop_through_temporary_file_and_download () {
  for i in $(cat $temporary_list_repos); do
    download_or_pull_repo $i
  done
  page_loop=$(expr $page_loop + 1)
  download_repos_from_page $page_loop
}

ask_local_path_to_download () {
  read -p "Please, provides the local path for download: " local_path_to_download
  if [ ! -d $local_path_to_download ] || [[ $local_path_to_download = "" ]]
  then
    echo The provided folder path does not exists.
    ask_local_path_to_download
  fi
}

## Main function
downloadgithubrepos () {
  local temporary_list_repos=$(writes_temporary_list_repos_address)
  local page_loop=1
  local local_path_to_download
  read -p "Please, provides the github user alias: " github_user_alias
  local repository_address=https://api.github.com/users/$github_user_alias/repos
  ask_local_path_to_download
  cd $local_path_to_download
  download_repos_from_page $page_loop
}

## detect if being sourced and
## export if so else execute
## main function with args
if [[ /usr/local/bin/shellutil != /usr/local/bin/shellutil ]]; then
  export -f downloadgithubrepos
else
  downloadgithubrepos "${@}"
  exit 0
fi
