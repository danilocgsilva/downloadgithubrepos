BIN ?= downloadgithubrepos
PREFIX ?= /usr/local

install:
	cp downloadgithubrepos.sh $(PREFIX)/bin/$(BIN)
	chmod +x $(PREFIX)/bin/$(BIN)

uninstall:
	rm -f $(PREFIX)/bin/$(BIN)
